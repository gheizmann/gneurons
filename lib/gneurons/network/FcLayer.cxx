#include "FcLayer.hxx"
#include "NetworkSettings.hxx"
#include "GneuronsException.hxx"

Gneurons::FcLayer::FcLayer(int nbInputs, int nbOutputs) :
AbstractLayer(nbInputs, nbOutputs),
weights(nullptr),
bias(nullptr),
gradWeights(nullptr),
gradBias(nullptr)
{
    Gneurons::NetworkSettings &settings = Gneurons::NetworkSettings::getInstance();

    weights = new double*[nbOutputs];
    gradWeights = new double*[nbOutputs];
    for(int i(0); i<nbOutputs; ++i)
    {
        weights[i] = new double[nbInputs];
        gradWeights[i] = new double[nbInputs];
        for(int j(0); j<nbInputs; ++j)
        {
            weights[i][j] = settings.getRandom() - 0.5;
        }
    }
    bias = new double[nbOutputs];
    gradBias = new double[nbOutputs];
    for(int i(0); i<nbOutputs; ++i)
    {
        bias[i] = settings.getRandom() - 0.5;
    }
}

Gneurons::FcLayer::~FcLayer()
{
    if(weights != nullptr)
    {
        for(int i(0); i<nbOutputs; ++i)
        {
            delete[] weights[i];
        }
        delete[] weights;
        weights = nullptr;
    }
    if(bias != nullptr)
    {
        delete[] bias;
        bias = nullptr;
    }
    if(gradWeights != nullptr)
    {
        for(int i(0); i<nbOutputs; ++i)
        {
            delete[] gradWeights[i];
        }
        delete[] gradWeights;
        gradWeights = nullptr;
    }
    if(gradBias != nullptr)
    {
        delete[] gradBias;
        gradBias = nullptr;
    }
}

const std::vector<double> Gneurons::FcLayer::forward(const std::vector<double> &x)
{
    for(std::vector<double>::iterator it(outLayer.begin()); it != outLayer.end(); ++it) {
        *it = 0.0;
    }

    if(x.size() != nbInputs)
    {
        throw Gneurons::Exception("Vector dimension must be the same as the input ,neural layer!", __FILE__, __LINE__);
    }
    for(int i(0); i<nbInputs; ++i)
    {
        entries[i] = x[i];
    }
    for(int j(0); j<nbOutputs; ++j)
    {
        for(int i(0); i<nbInputs; ++i)
        {
            outLayer[j] += weights[j][i]*entries[i];
        }
        outLayer[j] += bias[j];
    }

    return outLayer;
}

const std::vector<double> Gneurons::FcLayer::backward(const std::vector<double> &gradY)
{
    Gneurons::NetworkSettings &settings = Gneurons::NetworkSettings::getInstance();

    for(std::vector<double>::iterator it(inLayer.begin()); it != inLayer.end(); ++it) {
        *it = 0.0;
    }

    if(gradY.size() != nbOutputs)
    {
        throw Gneurons::Exception("Vector dimension must be the same as the output neural layer!", __FILE__, __LINE__);
    }
    for(int i(0); i<nbInputs; ++i)
    {
        for(int j(0); j<nbOutputs; ++j)
        {
            inLayer[i] += weights[j][i]*gradY[j];
        }
    }
    for(int i(0); i<nbOutputs; ++i)
    {
        for(int j(0); j<nbInputs; ++j)
        {
            gradWeights[i][j] = gradY[i]*entries[j];
        }
    }
    for(int i(0); i<nbOutputs; ++i)
    {
        gradBias[i] = gradY[i];
    }
    for(int i(0); i<nbOutputs; ++i)
    {
        for(int j(0); j<nbInputs; ++j)
        {
            weights[i][j] -= settings.getLearningRate()*gradWeights[i][j];
            weights[i][j] += 2.0*(settings.getRandom()-0.5)*settings.getNoise();
        }
    }
    for(int i(0); i<nbOutputs; ++i)
    {
        bias[i] -= settings.getLearningRate()*gradBias[i];
        bias[i] += 2.0*(settings.getRandom()-0.5)*settings.getNoise();
    }

    return inLayer;
}
