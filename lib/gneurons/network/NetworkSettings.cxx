#include "NetworkSettings.hxx"
#include "GneuronsException.hxx"
#include <cmath>
#include <ctime>
#include <cstdlib>

Gneurons::NetworkSettings Gneurons::NetworkSettings::instance = Gneurons::NetworkSettings();

Gneurons::NetworkSettings &Gneurons::NetworkSettings::operator=(Gneurons::NetworkSettings &&networkSettings)
{
    setLossFunction();

    return * this;
}

Gneurons::NetworkSettings::NetworkSettings(Gneurons::NetworkSettings &&networkSettings)
{
    setLossFunction();
}

Gneurons::NetworkSettings::NetworkSettings() :
learningRate(Gneurons::DEFAULT_LEARNING_RATE),
noise(0.0)
{
    std::srand(std::time(NULL));
    setLossFunction();
}

Gneurons::NetworkSettings::~NetworkSettings()
{}

Gneurons::NetworkSettings &Gneurons::NetworkSettings::getInstance()
{
    return Gneurons::NetworkSettings::instance;
}

double Gneurons::NetworkSettings::getRandom() const
{
    return (double)std::rand() / (double)RAND_MAX;
}

void Gneurons::NetworkSettings::setLearningRate(double learningRate)
{
    if(learningRate <= 0.0)
    {
        throw Gneurons::Exception("The learning rate must be a >0 number!", __FILE__, __LINE__);
    }
    this->learningRate = learningRate;
}

double Gneurons::NetworkSettings::getLearningRate() const
{
    return learningRate;
}

void Gneurons::NetworkSettings::setLossFunction(Gneurons::LossFuncType type)
{
    switch(type)
    {
        case Gneurons::LossFuncType::EUCLIDIAN_NORM:
            lossFunc = [](const std::vector<double> &yCalc, const std::vector<double> &yPred) -> double
            {
                double ret { 0.0 };

                for(int i(0); i<yCalc.size(); ++i)
                {
                    ret += std::pow(yCalc[i]-yPred[i], 2.0);
                }
                ret /= (yCalc.size()*2.0);

                return ret;
            };
            lossFuncPrime = [](const std::vector<double> &yCalc, const std::vector<double> &yPred) -> const std::vector<double>
            {
                std::vector<double> ret(yCalc.size());
                
                for(int i(0); i<yCalc.size(); ++i)
                {
                    ret[i] = (yCalc[i]-yPred[i])/yCalc.size();
                }

                return ret;
            };
            break;
        default:
            break;
    }
}

double Gneurons::NetworkSettings::calcLoss(const std::vector<double> &yCalc, const std::vector<double> &yPred) const
{
    if(yCalc.size() != yPred.size())
    {
        throw Gneurons::Exception("Vectors have to the same dimension!", __FILE__, __LINE__);
    }

    return lossFunc(yCalc, yPred);
}

const std::vector<double> Gneurons::NetworkSettings::calcLossPrime(const std::vector<double> &yCalc, const std::vector<double> &yPred) const
{
        if(yCalc.size() != yPred.size())
    {
        throw Gneurons::Exception("Vectors have to the same dimension!", __FILE__, __LINE__);
    }

    return lossFuncPrime(yCalc, yPred);
}

void Gneurons::NetworkSettings::setNoise(double noise)
{
    this->noise = std::abs(noise);
}

double Gneurons::NetworkSettings::getNoise() const
{
    return noise;
}
