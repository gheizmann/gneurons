#include "ActivationLayer.hxx"
#include "GneuronsException.hxx"

Gneurons::ActivationLayer::ActivationLayer(int nbInputs, Gneurons::ActivFuncType type) :
AbstractLayer(nbInputs, nbInputs),
activFunc(type)
{}

Gneurons::ActivationLayer::~ActivationLayer()
{}

const std::vector<double> Gneurons::ActivationLayer::forward(const std::vector<double> &x)
{
    if(x.size() != nbInputs)
    {
        throw Gneurons::Exception("Input vector dimension must be the same as the number of neuron in the neural layer!", __FILE__, __LINE__);
    }
    for(int i(0); i<nbOutputs; ++i)
    {
        entries[i] = x[i];
        outLayer[i] = activFunc.calc(x[i]);
    }

    return outLayer;
}

const std::vector<double> Gneurons::ActivationLayer::backward(const std::vector<double> &gradY)
{
    if(gradY.size() != nbOutputs)
    {
        throw Gneurons::Exception("Output gradient vector dimension must be the same as the number of neuron in the neural layer!", __FILE__, __LINE__);
    }
    for(int i(0); i<nbInputs; ++i)
    {
        inLayer[i] = gradY[i]*activFunc.calcPrime(entries[i]);
    }

    return inLayer;
}
