#ifndef ACTIV_FUNC_TYPES_HXX__
#define ACTIV_FUNC_TYPES_HXX__

/*!
 * \file ActivFuncTypes.hxx
 * \brief Fichier d'en-tête contenant des données (types) pour les fonctions d'activation
 * 
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-11
 * \version 0.1
 */

/*!
 * \namespace Gneurons
 * \brief Espace de nom de la bibliothèque Gneurons
 */
namespace Gneurons
{
/*!
 * \enum ActivFuncType
 * \brief Énumération de description des différents types de fonctions d'activation utilisables
 */
enum ActivFuncType {
    SIGMOID, /* Fonction d'activation sigmoïde */
    HYPERBOLIC_TANGENT /* Fonction d'activation tangente hyperbolique */
};
};

#endif // ACTIV_FUNC_TYPES_HXX__
