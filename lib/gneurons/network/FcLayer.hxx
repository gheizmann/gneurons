#ifndef FC_LAYER_HXX__
#define FC_LAYER_HXX__

/*!
 * \file FcLayer.hxx
 * \brief Fichier d'en-tête décrivant une couche de neurones "fully connected"
 * 
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-11
 * \version 0.1
 */

#include "AbstractLayer.hxx"

/*!
 * \namespace Gneurons
 * \brief Espace de nom de la bibliothèque Gneurons
 */
namespace Gneurons
{
/*!
 * \class FcLayer
 * \brief Cette classe décrit une couche de neurones "fully connected"
 */
class FcLayer : public AbstractLayer
{
private:
    double **weights; /* Poids de la couche de neurones */
    double *bias; /* Biais de la couche de neurones */
    double **gradWeights; /* Gradient des poids de la couche de neurones */
    double *gradBias; /* Gradient des biais de la couche de neurones */
public:
    /*!
     * \brief Constructeur
     * \param nbInputs Nombre d'entrées de la couche
     * \param nbOutputs Nombre de sorties de la couche
     */
    FcLayer(int nbInputs, int nbOutputs);
    /*!
     * \brief Destructeur
     */
    virtual ~FcLayer();
    /*!
     * \brief Méthode calculant la sortie de la couche en fonction d'une entrée fournie
     * \param x Le vecteur formant l'entrée fournie à la couche de neurones
     * \return Un vecteur décrivant la sortie de la couche
     * \exception Lance une exception Gneurons::Exception si le vecteur x n'a pas la bonne dimension
     */
    virtual const std::vector<double> forward(const std::vector<double> &x);
    /*!
     * \brief Méthode calculant le gradient de l'entrée de la couche en fonction du gradient de la sortie,
     * et mise à jour des gradients de poids et du gradient des biais
     * \param gradY Gradient de la sortie de la couche
     * \return Le gradient gradX de l'entrée correspondant
     * \exception Lance une exception Gneurons::Exception si le vecteur gradY n'a pas la bonne dimension
     */
    virtual const std::vector<double> backward(const std::vector<double> &gradY);
};
};

#endif // FC_LAYER_HXX__
