#include "ActivFunc.hxx"
#include <cmath>

Gneurons::ActivFunc::ActivFunc(Gneurons::ActivFuncType type) :
activation(),
activationPrime()
{
    setActivation(type);
}

void Gneurons::ActivFunc::setActivation(Gneurons::ActivFuncType type)
{
    switch(type)
    {
    case Gneurons::ActivFuncType::SIGMOID:
        activation = [](double x) -> double
        {
            return 1.0/(1.0+std::exp(-x));
        };
        activationPrime = [](double x) -> double
        {
            return std::exp(-x)/std::pow(1.0+std::exp(-x), 2.0);
        };
        break;
    case Gneurons::ActivFuncType::HYPERBOLIC_TANGENT:
        activation = [](double x) -> double
        {
            return std::tanh(x);
        };
        activationPrime = [](double x) -> double
        {
            return 1.0/std::pow(std::cosh(x), 2.0);
        };
        break;
    default:
        break;
    }
}

double Gneurons::ActivFunc::calc(double x) const
{
    return activation(x);
}

double Gneurons::ActivFunc::calcPrime(double x) const
{
    return activationPrime(x);
}
