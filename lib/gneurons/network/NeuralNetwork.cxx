#include "NeuralNetwork.hxx"
#include "GneuronsException.hxx"
#include "NetworkSettings.hxx"

Gneurons::NeuralNetwork::NeuralNetwork()
{}

Gneurons::NeuralNetwork::~NeuralNetwork()
{}

void Gneurons::NeuralNetwork::add(Gneurons::AbstractLayer *layer)
{
    layers.push_back(std::unique_ptr<AbstractLayer>(layer));
}

const std::vector<double> Gneurons::NeuralNetwork::predict(const std::vector<double> &input) const
{
    std::vector<double> output;

    if(layers.empty())
    {
        throw Gneurons::Exception("There is no neural layer in the neural network!", __FILE__, __LINE__);
    }
    output = input;
    for(int i(0); i<layers.size(); ++i)
    {
        output = layers[i]->forward(output);
    }

    return output;
}

void Gneurons::NeuralNetwork::fit(const std::vector<double> &input, const std::vector<double> &output)
{
    Gneurons::NetworkSettings &settings = Gneurons::NetworkSettings::getInstance();
    std::vector<double> outputCalc;
    std::vector<double> errorPrime;

    if(layers.empty())
    {
        throw Gneurons::Exception("There is no neural layer in the neural network!", __FILE__, __LINE__);
    }
    outputCalc = predict(input);
    errorPrime = settings.calcLossPrime(outputCalc, output);
    for(int i(layers.size()-1); i>=0; --i)
    {
        errorPrime = layers[i]->backward(errorPrime);
    }
}
