#ifndef GNEURONS_EXCEPTION_HXX__
#define GNEURONS_EXCEPTION_HXX__

/*!
 * \file GneuronsException.hxx
 * \brief Fichier d'en-tête permettant de décrire une exception personnalisée pour la librairie Gneurons.
 * 
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-11
 * \version 0.1
 */

#include <exception>
#include <string>

/*!
 * \namespace Gneurons
 * \brief Espace de nom de la bibliothèque Gneurons
 */
namespace Gneurons
{
/*!
 * \class Exception
 * \brief Classe décrivant une exception personnalisée
 */
class Exception : public std::exception
{
private:
    std::string msg; /* Message personnalisé de l'exception */
public:
    /*!
     * \brief Constructeur
     * \param msg Le message personnalisé de l'exception
     * \param file Le nom du fichier à partir duquel est lancé l'exception (par défaut : "" si pas de fichier renseigné)
     * \param line La ligne à partir de laquelle est lancée l'exception (par défaut : -1 si pas de ligne renseignée)
     */
    Exception(const std::string &msg, const std::string &file = "", int line = -1) noexcept;
    /*!
     * \brief Destructeur
     */
    virtual ~Exception() noexcept;
    /*!
     * \brief Redéfinition de la méthode what() pour obtenir le message personnalisé de l'exception
     * \return Un pointeur vers le message de l'exception
     */
    virtual const char *what() const noexcept;
};
};

#endif // GNEURONS_EXCEPTION_HXX__
