#ifndef ABSTRACT_LAYER_HXX__
#define ABSTRACT_LAYER_HXX__

/*!
 * \file AbstractLayer.hxx
 * \brief Fichier d'en-tête décrivant une couche de neurones abstraite.
 * 
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-11
 * \version 0.1
 */

#include <vector>

/*!
 * \namespace Gneurons
 * \brief Espace de nom de la bibliothèque Gneurons
 */
namespace Gneurons
{
/*!
 * \class AbstractLayer
 * \brief Classe abstraite décrivant une couche de neurones
 */
class AbstractLayer
{
protected:
    int nbInputs; /* Nombre de neurones d'entrée */
    int nbOutputs; /* Nombre de neurones de sortie */
    double *entries; /* Vecteur d'entrée sauvegardé de la couche (lors de l'appel à forward()) */
    std::vector<double> inLayer; /* Vecteur (alloué) qui contient l'entrée renvoyée par la méthode backward */
    std::vector<double> outLayer; /* Vecteur (alloué) qui contient la sortie renvoyée par la méthode forward */
public:
    /*!
     * \brief Constructeur
     * \param nbInputs Nombre de neurones d'entrée
     * \param nbOutputs Nombre de neurones de sortie
     * \exception Lance une exception Gneurons::Exception si l'un des deux nombres est <=0
     */
    AbstractLayer(int nbInputs, int nbOutputs);
    /*!
     * \brief Destructeur
     */
    virtual ~AbstractLayer();
    /*!
     * \brief Méthode calculant la sortie de la couche en fonction d'une entrée fournie
     * \param x Le vecteur formant l'entrée fournie à la couche de neurones
     * \return Un vecteur décrivant la sortie de la couche
     * \exception Lance une exception Gneurons::Exception si le vecteur x n'a pas la bonne dimension
     */
    virtual const std::vector<double> forward(const std::vector<double> &x) = 0;
    /*!
     * \brief Méthode calculant le gradient de l'entrée de la couche en fonction du gradient de la sortie
     * \param gradY Gradient de la sortie de la couche
     * \return Le gradient gradX de l'entrée correspondant
     * \exception Lance une exception Gneurons::Exception si le vecteur gradY n'a pas la bonne dimension
     */
    virtual const std::vector<double> backward(const std::vector<double> &gradY) = 0;
};
};

#endif // ABSTRACT_LAYER_HXX__
