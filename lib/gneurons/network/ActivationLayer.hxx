#ifndef ACTIVATION_LAYER_HXX__
#define ACTIVATION_LAYER_HXX__

/*!
 * \file ActivationLayer.hxx
 * \brief Fichier d'en-tête permettant de décrire une couche de neurones d'activation
 * 
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-11
 * \version 0.1
 */

#include "AbstractLayer.hxx"
#include "ActivFunc.hxx"
#include "ActivFuncTypes.hxx"

/*!
 * \namespace Gneurons
 * \brief Espace de nom de la bibliothèque Gneurons
 */
namespace Gneurons
{
/*!
 * \class ActivationLayer
 * \brief Classe décrivant une couche de neurones d'activation
 */
class ActivationLayer : public Gneurons::AbstractLayer
{
private:
    ActivFunc activFunc; /* La fonction d'activation utilisée pour cette couche de neurones */
public:
    /*!
     * \brief Constructeur
     * \param nbInputs Nombre de neurones de la couche
     * \param type Type de la fonction d'activation à utiliser (par défaut : la fonction sigmoïde)
     */
    ActivationLayer(int nbInputs, Gneurons::ActivFuncType type = Gneurons::ActivFuncType::SIGMOID);
    /*!
     * \brief Destructeur
     */
    virtual ~ActivationLayer();
    /*!
     * \brief Méthode calculant la sortie de la couche en fonction d'une entrée fournie
     * \param x Le vecteur formant l'entrée fournie à la couche de neurones
     * \return Un vecteur décrivant la sortie de la couche
     * \exception Lance une exception Gneurons::Exception si le vecteur x n'a pas la bonne dimension
     */
    virtual const std::vector<double> forward(const std::vector<double> &x);
    /*!
     * \brief Méthode calculant le gradient de l'entrée de la couche en fonction du gradient de la sortie
     * \param gradY Gradient de la sortie de la couche
     * \return Le gradient gradX de l'entrée correspondant
     * \exception Lance une exception Gneurons::Exception si le vecteur gradY n'a pas la bonne dimension
     */
    virtual const std::vector<double> backward(const std::vector<double> &gradY);
};
};

#endif // ACTIVATION_LAYER_HXX__
