#include "AbstractLayer.hxx"
#include "GneuronsException.hxx"

Gneurons::AbstractLayer::AbstractLayer(int nbInputs, int nbOutputs) :
nbInputs(nbInputs),
nbOutputs(nbOutputs),
entries(nullptr),
inLayer(std::vector<double>(nbInputs)),
outLayer(std::vector<double>(nbOutputs))
{
    if(nbInputs <= 0 || nbOutputs <= 0)
    {
        throw Gneurons::Exception("Invalid number of input or output for the neural layer!", __FILE__, __LINE__);
    }
    entries = new double[nbInputs];
}

Gneurons::AbstractLayer::~AbstractLayer()
{
    if(entries != nullptr)
    {
        delete[] entries;
        entries = nullptr;
    }
}
