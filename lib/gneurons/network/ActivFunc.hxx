#ifndef ACTIV_FUNC_HXX__
#define ACTIV_FUNC_HXX__

/*!
 * \file ActivFunc.hxx
 * \brief Fichier d'en-tête décrivant une fonction d'activation
 * 
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-11
 * \version 0.1
 */

#include <functional>
#include "ActivFuncTypes.hxx"

/*!
 * \namespace Gneurons
 * \brief Espace de nom de la bibliothèque Gneurons
 */
namespace Gneurons
{
/*!
 * \class ActivFunc
 * \brief Classe décrivant une fonction d'activation
 */
class ActivFunc
{
private:
    std::function<double(double)> activation; /* La fonction d'activation stockée */
    std::function<double(double)> activationPrime; /* La dérivée de la fonction d'activation stockée */
public:
    /*!
     * \brief Constructeur
     * \param type Type de fonction d'activation à utiliser (par défaut : une fonction sigmoïde)
     */
    ActivFunc(ActivFuncType type = Gneurons::ActivFuncType::SIGMOID);
    /*!
     * \brief Setter pour la fonction d'activation
     * \param type Type de fonction d'activation à utiliser
     */
    void setActivation(ActivFuncType type);
    /*!
     * \brief Calcul de la fonction d'activation étant donné un paramètre
     * \param x La valeur en laquelle évaluer la fonction d'activation
     * \return La valeur calculée
     */
    double calc(double x) const;
    /*!
     * \brief Calcul de la dérivée de la fonction d'activation étant donné un paramètre
     * \param x La valeur en laquelle évaluer la dérivée de la fonction d'activation
     * \return La valeur calculée
     */
    double calcPrime(double x) const;
};
};

#endif // ACTIV_FUNC_HXX__
