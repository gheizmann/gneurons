#ifndef NEURAL_NETWORK_HXX__
#define NEURAL_NETWORK_HXX__

/*!
 * \file NeuralNetwork.hxx
 * \brief Fichier d'en-tête décrivant un réseau de neurones
 * 
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-11
 * \version 0.1
 */

#include <vector>
#include <memory>
#include "AbstractLayer.hxx"

/*!
 * \namespace Gneurons
 * \brief Espace de nom de la bibliothèque Gneurons
 */
namespace Gneurons
{
/*!
 * \class NeuralNetwork
 * \brief Classe décrivant un réseau de neurones
 */
class NeuralNetwork
{
private:
    std::vector<std::unique_ptr<Gneurons::AbstractLayer>> layers; /* Couches de neurone du réseau */
public:
    /*!
     * \brief Constructeur
     */
    NeuralNetwork();
    /*!
     * \brief Destructeur
     */
    ~NeuralNetwork();
    /*!
     * \brief Méthode d'ajout d'une couche de neurones
     * \param layer La couche à ajouter
     */
    void add(AbstractLayer *layer);
    /*!
     * \brief Calcul de la sortie du réseau de neurones en fonction d'une entrée
     * \param input Vecteur représentant l'entrée du réseau de neurones
     * \return Un vecteur représentant la sortie calculée du réseau
     * \exception Lance une exception Gneurons::Exception s'il n'y a pas de couches
     */
    const std::vector<double> predict(const std::vector<double> &input) const;
    /*!
     * \brief Méthode appliquant la méthode de descente du gradient
     * \param input Vecteur contenant une donnée d'entraînement
     * \param output Vecteur contenant la sortie attendue
     * \exception Lance une exception Gneurons::Exception s'il n'y a pas de couches
     */
    void fit(const std::vector<double> &input, const std::vector<double> &output);
};
};

#endif // NEURAL_NETWORK_HXX__
