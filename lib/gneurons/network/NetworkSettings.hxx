#ifndef NETWORK_SETTINGS_HXX__
#define NETWORK_SETTINGS_HXX__

/*!
 * \file NetworkSettings.hxx
 * \brief Fichier d'en-tête décrivant le paramétrage du réseau de neurones.
 * Permet également de définir quelques fonctions servant à décrire la
 * mathématique du réseau de neurones (fonction de perte, nombres aléatoires). 
 * 
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-11
 * \version 0.2
 */

#include <functional>
#include <vector>

/*!
 * \namespace Gneurons
 * \brief Espace de nom de la bibliothèque Gneurons
 */
namespace Gneurons
{
/*!
 * \enum LossFuncType
 * \brief Énumération donnant les types de fonctions d'apprentissage possibles
 */
enum LossFuncType
{
    EUCLIDIAN_NORM /* Norme euclidienne */
};

const double DEFAULT_LEARNING_RATE { 0.001 }; /* Taux d'apprentissage par défaut */

/*!
 * \class NetworkSettings
 * \brief Classe singleton de description des paramètres d'un réseau de neurones
 */
class NetworkSettings
{
private:
    static NetworkSettings instance; /* Instance de la classe */
    double learningRate; /* Taux d'apprentissage du réseau de neurones */
    double noise; /* Bruit (injection dans les màj des poids et des biais des neurones) */
    std::function<double(const std::vector<double> &, const std::vector<double> &)> lossFunc; /* Fonction d'apprentissage */
    std::function<const std::vector<double>(const std::vector<double> &, const std::vector<double> &)> lossFuncPrime; /* Gradient de la fonction d'apprentissage */
    /*!
     * \brief Constructeur (privé)
     */
    NetworkSettings();
    /*!
     * \brief Opérateur de copie privé
     * \param networkSettings L'objet à déplacer
     * \return Une référence vers l'objet déplacé
     */
    NetworkSettings &operator=(NetworkSettings &&);
    /*!
     * \brief Constructeur de déplacement
     */
    NetworkSettings(NetworkSettings &&);
public:
    NetworkSettings &operator=(const NetworkSettings &networkSettings) = delete;
    NetworkSettings(const NetworkSettings &) = delete;
    /*!
     * \brief Destructeur
     */
    ~NetworkSettings();
    /*!
     * \brief Permet d'obtenir une instance de la classe
     * \return Une référence vers l'instance de la classe
     */
    static NetworkSettings &getInstance();
    /*!
     * \brief Obtenir un nombre aléatoire entre 0.0 et 1.0
     * \return Un nombre aléatoire entre 0.0 et 1.0
     */
    double getRandom() const;
    /*!
     * \brief Setter pour le taux d'apprentissage
     * \param learningRate Le taux d'apprentissage à setter
     * \exception Lancer une exception Gneurons::Exception si le taux renseigné est <= 0.0
     */
    void setLearningRate(double learningRate);
    /*!
     * \brief Getter du taux d'apprentissage
     * \return Le taux d'apprentissage courant
     */
    double getLearningRate() const;
    /*!
     * \brief Setter pour la fonction d'apprentissage
     * \param type Type de la fonction d'apprentissage à utiliser (par défaut : la norme euclidienne)
     */
    void setLossFunction(LossFuncType type = EUCLIDIAN_NORM);
    /*!
     * \brief Calcul de la valeur d'apprentissage
     * \param yCalc Vecteur donnant la valeur calculée
     * \param yPred Vecteur donnant la valeur fournie lors de l'apprentissage
     * \return La valeur d'apprentissage
     * \exception Renvoie une exception Gneurons::Exception si les dimensions des vecteurs fournis en entrée ne correspondent pas
     */
    double calcLoss(const std::vector<double> &yCalc, const std::vector<double> &yPred) const;
    /*!
     * \brief Calcul du gragient de la valeur d'apprentissage
     * \param yCalc Vecteur donnant la valeur calculée
     * \param yPred Vecteur donnant la valeur fournie lors de l'apprentissage
     * \return Le gradient de la valeur d'apprentissage sous la forme d'un vecteur
     * \exception Renvoie une exception Gneurons::Exception si les dimensions des vecteurs fournis en entrée ne correspondent pas
     */
    const std::vector<double> calcLossPrime(const std::vector<double> &yCalc, const std::vector<double> &yPred) const;
    /*!
     * \brief Setter pour le bruit
     * \param noise Valeur du bruit à setter
     */
    void setNoise(double noise);
    /*!
     * \brief Getter pour le bruit
     * \return Le bruit enregistré (par défaut : 0.0)
     */
    double getNoise() const;
};
};

#endif // NETWORK_SETTINGS_HXX__
