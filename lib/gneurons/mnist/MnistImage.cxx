#include "MnistImage.hxx"
#include "MnistVars.hxx"
#include "MnistException.hxx"
#include "MnistCurl.hxx"
#include <fstream>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <functional>
#include <sstream>

MnistImage::MnistImage() :
mnistData(nullptr)
{}

MnistImage::~MnistImage()
{
    clear();
}

void MnistImage::load(const std::string &fullPath)
{
    std::ifstream file;
    boost::iostreams::filtering_streambuf<boost::iostreams::input> inbuf;
    std::istream instream(&inbuf);
    std::function<int(int)> reverseInt =
    [](int i)
    {
        uchar c1, c2, c3, c4;

        c1 = i & 255;
        c2 = (i >> 8) & 255;
        c3 = (i >> 16) & 255;
        c4 = (i >> 24) & 255;

        return ((int)c1 << 24) + ((int)c2 << 16) + ((int)c3 << 8) + c4;
    };
    int magicNumber = 0;
    std::ostringstream ossErr;

    if(mnistData != nullptr)
    {
        throw MnistException("Image datas has been already loaded!", __FILE__, __LINE__);
    }
    if(!MnistCurl::isDownloaded(fullPath))
    {
        throw MnistException("The file has not been downloaded!", __FILE__, __LINE__);
    }
    file.open(fullPath, std::ios_base::in | std::ios_base::binary); 
    inbuf.push(boost::iostreams::gzip_decompressor());
    inbuf.push(file);
    instream.read((char *)&magicNumber, sizeof(magicNumber));
    magicNumber = reverseInt(magicNumber);
    if(magicNumber != MNIST_IMAGE_MAGIC_NUMBER)
    {
        file.close();
        ossErr << "Bad magic number for file: " << fullPath <<
        ", magic number is: " << magicNumber << " and it should be: " << MNIST_IMAGE_MAGIC_NUMBER;
        throw MnistException(ossErr.str(), __FILE__, __LINE__);
    }
    instream.read((char *)&nImages, sizeof(nImages));
    nImages = reverseInt(nImages);
    instream.read((char *)&nRows, sizeof(nRows));
    nRows = reverseInt(nRows);
    instream.read((char *)&nCols, sizeof(nCols));
    nCols = reverseInt(nCols);
    imageSize = nRows*nCols;
    mnistData = new uchar*[nImages];
    for(int i(0); i<nImages; ++i)
    {
        mnistData[i] = new uchar[imageSize];
        instream.read((char *)mnistData[i], imageSize);
    }
    file.close();
}

void MnistImage::clear()
{
    if(mnistData != nullptr)
    {
        for(int i(0); i<nImages; ++i)
        {
            delete[] mnistData[i];
        }
        delete[] mnistData;
        mnistData = nullptr;
    }
}

const std::vector<uchar> MnistImage::operator[](int idx) const
{
    std::vector<uchar> ret;

    if(mnistData == nullptr)
    {
        throw MnistException("No MNIST images loaded!", __FILE__, __LINE__);
    }
    if(idx < 0 || idx >= nImages)
    {
        throw MnistException("idx is out of range!", __FILE__, __LINE__);
    }
    for(int i(0); i<imageSize; ++i)
    {
        ret.push_back(mnistData[idx][i]);
    }

    return ret;
}

int MnistImage::getNbRows() const
{
    int ret { -1 };

    if(mnistData != nullptr)
    {
        ret = nRows;
    }

    return ret;
}

int MnistImage::getNbCols() const
{
    int ret { -1 };

    if(mnistData != nullptr)
    {
        ret = nCols;
    }

    return ret;
}

int MnistImage::getNbImages() const
{
    int ret { -1 };

    if(mnistData != nullptr)
    {
        ret = nImages;
    }

    return ret;
}

int MnistImage::getImageSize() const
{
    int ret { -1 };

    if(mnistData != nullptr)
    {
        ret = imageSize;
    }

    return ret;
}
