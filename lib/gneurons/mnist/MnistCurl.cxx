#include "MnistCurl.hxx"
#include <curl/curl.h>
#include <fstream>

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
    size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);

    return written;
}

void MnistCurl::download(const std::string &urlName, const std::string &fileName)
{
    CURL *curl;
    FILE *file;
    CURLcode res;

    curl = curl_easy_init();
    if(curl)
    {
        file = fopen(fileName.c_str(), "wb");
        curl_easy_setopt(curl, CURLOPT_URL, (urlName+fileName).c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        fclose(file);
    }
}

bool MnistCurl::isDownloaded(const std::string &fileName)
{
    bool ret { false };
    std::ifstream file;

    file.open(fileName.c_str());
    if(file.good())
    {
        ret = true;
        file.close();
    }

    return ret;
}
