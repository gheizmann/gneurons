#include "MnistLabel.hxx"
#include "MnistVars.hxx"
#include "MnistException.hxx"
#include "MnistCurl.hxx"
#include <fstream>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <functional>
#include <sstream>

MnistLabel::MnistLabel() :
mnistData(nullptr)
{}

MnistLabel::~MnistLabel()
{
    clear();
}

void MnistLabel::load(const std::string &fullPath)
{
    std::ifstream file;
    boost::iostreams::filtering_streambuf<boost::iostreams::input> inbuf;
    std::istream instream(&inbuf);
    std::function<int(int)> reverseInt =
    [](int i)
    {
        uchar c1, c2, c3, c4;

        c1 = i & 255;
        c2 = (i >> 8) & 255;
        c3 = (i >> 16) & 255;
        c4 = (i >> 24) & 255;

        return ((int)c1 << 24) + ((int)c2 << 16) + ((int)c3 << 8) + c4;
    };
    int magicNumber = 0;
    std::ostringstream ossErr;

    if(mnistData != nullptr)
    {
        throw MnistException("Label datas has been already loaded!", __FILE__, __LINE__);
    }
    if(!MnistCurl::isDownloaded(fullPath))
    {
        throw MnistException("The file has not been downloaded!", __FILE__, __LINE__);
    }
    file.open(fullPath, std::ios_base::in | std::ios_base::binary); 
    inbuf.push(boost::iostreams::gzip_decompressor());
    inbuf.push(file);
    instream.read((char *)&magicNumber, sizeof(magicNumber));
    magicNumber = reverseInt(magicNumber);
    if(magicNumber != MNIST_LABEL_MAGIC_NUMBER)
    {
        file.close();
        ossErr << "Bad magic number for file: " << fullPath <<
        ", magic number is: " << magicNumber << " and it should be: " << MNIST_LABEL_MAGIC_NUMBER;
        throw MnistException(ossErr.str(), __FILE__, __LINE__);
    }
    instream.read((char *)&nLabels, sizeof(nLabels));
    nLabels = reverseInt(nLabels);
    mnistData = new uchar[nLabels];
    for(int i(0); i<nLabels; ++i)
    {
        instream.read((char *)&mnistData[i], 1);
    }
    file.close();
}

void MnistLabel::clear()
{
    if(mnistData != nullptr)
    {
        delete[] mnistData;
        mnistData = nullptr;
    }
}

const std::vector<bool> MnistLabel::operator[](int idx) const
{
    std::vector<bool> ret(4);
    uchar val;

    if(mnistData == nullptr)
    {
        throw MnistException("No data labels loaded!", __FILE__, __LINE__);
    }
    if(idx < 0 || idx >= nLabels)
    {
        throw MnistException("idx is out of range!", __FILE__, __LINE__);
    }
    val = mnistData[idx];
    for(int i(3); i>=0; --i)
    {
        ret[i] = val % 2;
        val /= 2;
    }

    return ret;
}

int MnistLabel::getLabel(int idx) const
{
    if(mnistData == nullptr)
    {
        throw MnistException("No data label loaded!", __FILE__, __LINE__);
    }
    if(idx < 0 || idx >= nLabels)
    {
        throw MnistException("idx is out of range!", __FILE__, __LINE__);
    }

    return (int)mnistData[idx];
}

int MnistLabel::getNbLabels() const
{
    int ret { -1 };

    if(mnistData != nullptr)
    {
        ret = nLabels;
    }

    return ret;
}
