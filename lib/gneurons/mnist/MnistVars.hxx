#ifndef MNIST_VARS_HXX__
#define MNIST_VARS_HXX__

/*!
 * \file MnistVars.hxx
 * \brief Fichier d'en-tête contenant différentes constantes définissant les
 * chemins de téléchargement des fichiers du MNIST.
 * 
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-10
 * \version 0.1
 */

#include <string>

const int MNIST_IMAGE_MAGIC_NUMBER = 2051; /* Magic number des fichiers de données images du MNIST */
const int MNIST_LABEL_MAGIC_NUMBER = 2049; /* Magic number des fichiers de données labels du MNIST */

const std::string MNIST_URL_NAME = "http://yann.lecun.com/exdb/mnist/"; /* Constante contenant le nom de l'URL des fichiers à télécharger du MNIST */

const std::string MNIST_IMAGES_NAME[] = /* Constante contenant un tableau donnant le nom des fichiers contenant les images du MNIST */
{
    "train-images-idx3-ubyte.gz",
    "t10k-images-idx3-ubyte.gz"
};
const std::string MNIST_LABELS_NAME[] = /* Constante contenant un tableau donnant le nom des fichiers contenant les labels du MNIST */
{
    "train-labels-idx1-ubyte.gz",
    "t10k-labels-idx1-ubyte.gz"
};

#endif // MNIST_VARS_HXX__
