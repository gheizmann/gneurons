#ifndef MNIST_EXCEPTION_HXX__
#define MNIST_EXCEPTION_HXX__

/*!
 * \file MnistException.hxx
 * \brief Fichier d'en-tête décrivant une exception personnalisée pour la lecture des fichiers du MNIST
 * 
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-10
 * \version 0.1
 */

#include <string>
#include <exception>

/*!
 * \class MnistException
 * \brief Classe décrivant une exception personnalisée utilisée lors de la lecture (méthode load())
 * des données issues du MNIST
 */
class MnistException : public std::exception
{
private:
    std::string msg; /* Message personnalisé */
public:
    /*!
     * \brief Constructeur
     * \param msg Message personnalisé
     * \param file Le nom du fichier d'où provient l'exception (par défaut : "" si pas de fichier renseigné)
     * \param line La ligne à partir de laquelle a été lancée l'exception (par défaut : -1 si pas de ligne renseignée)
     */
    MnistException(const std::string &msg, const std::string &file = "", int line = -1) noexcept;
    /*!
     * \brief Destructeur
     */
    virtual ~MnistException() noexcept;
    /*!
     * \brief Redéfinition de what()
     * \return Le message de l'exception lancée
     */
    virtual const char *what() const noexcept;
};

#endif // MNIST_EXCEPTION_HXX__
