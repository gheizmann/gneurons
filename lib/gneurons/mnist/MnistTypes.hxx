#ifndef MNIST_TYPES_HXX__
#define MNIST_TYPES_HXX__

/*!
 * \file MnistTypes.hxx
 * \brief Fichier d'en-tête contenant des types prédéfinis pour la manipulation
 * de données issus du MNIST.
 *
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-10
 * \version 0.1
 */

typedef unsigned char uchar; /* Type personnalisé pour les caractères non signés */

#endif // MNIST_TYPES_HXX__
