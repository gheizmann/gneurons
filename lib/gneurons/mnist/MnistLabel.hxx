#ifndef MNIST_LABEL_HXX__
#define MNIST_LABEL_HXX__

/*!
 * \file MnistLabel.hxx
 * \brief Fichier d'en-tête permettant la manipulation de données (labels) du MNIST
 * 
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-12
 * \version 0.1
 */

#include <string>
#include <vector>
#include "MnistTypes.hxx"

/*!
 * \class MnistLabel
 * \brief Cette classe permet de stocker et de manipuler des données issues
 * de fichiers de labels du MNIST
 */
class MnistLabel
{
private:
    uchar *mnistData; /* Labels stockés du MNIST */
    int nLabels; /* Nombre de labels */
public:
    /*!
     * \brief Constructeur
     */
    MnistLabel();
    /*!
     * \brief Destructeur
     */
    ~MnistLabel();
    /*!
     * \brief Méthode de chargement des données de labels du MNIST
     * \param fullPath Chemin vers le fichier (compressé) du MNIST à charger
     * \exception Lance une exception MnistException si des données sont déjà chargées ou bien si le fichier n'existe pas ou bien si le fichier n'a pas le bon magic number
     */
    void load(const std::string &fullPath);
    /*!
     * \brief Vide les données chargées, s'il y en a
     */
    void clear();
    /*!
     * \brief Obtenir un label
     * \param idx Le numéro du label à obtenir
     * \return Un vecteur contenant la valeur du label (vecteur contenant la valeur binaire du label)
     * \exception Lance une exception MnistException si aucune donnée n'est chargée ou bien si idx est out of range
     */
    const std::vector<bool> operator[](int idx) const;
    /*!
     * \brief Obtenir le nombre du label
     * \param idx L'index du label à obtenir
     * \return La valeur numérique du label
     * \exception Lance une exception MnistException si aucune donnée n'est chargée ou bien si idx est out of range
     */
    int getLabel(int idx) const;
    /*!
     * \brief Obtenir le nombre de labels chargés
     * \return Le nombre de labels chargés (-1 si pas de labels chargés)
     */
    int getNbLabels() const;
};

#endif // MNIST_LABEL_HXX__
