#ifndef MNIST_CURL_HXX__
#define MNIST_CURL_HXX__

/*!
 * \file MnistCurl.hxx
 * \brief Fichier d'en-tête contenant des outils permettant de télécharger les
 * fichiers de données du MNIST.
 * 
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-10
 * \version 0.1
 */

#include <string>

/*!
 * \class MnistCurl
 * \brief Classe contenant des méthodes statiques pour télécharger des fichiers
 * de données du MNIST.
 */
class MnistCurl
{
public:
    /*!
     * \brief Méthode statique permettant de télécharger un fichier
     * \param urlName L'url de téléchargement
     * \param fileName Le nom du fichier à télécharger
     */
    static void download(const std::string &urlName, const std::string &fileName);
    /*!
     * \brief Méthode statique vérifiant si le fichier a déjà été téléchargé
     * \param fileName Nom du fichier dont on souhaite savoir s'il a été téléchargé ou non
     * \return true si le fichier a déjà été téléchargé, false sinon
     */
    static bool isDownloaded(const std::string &fileName);
};

#endif // MNIST_CURL_HXX__
