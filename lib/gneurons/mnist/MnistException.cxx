#include "MnistException.hxx"
#include <sstream>

MnistException::MnistException(const std::string &msg, const std::string &file, int line) noexcept :
msg("")
{
    std::ostringstream oss;

    oss << msg;
    if(file != "")
    {
        oss << "\n\tFrom file: " << file;
        if(line != -1)
        {
            oss << "\n\tAt line: " << line;
        }
    }

    this->msg = oss.str();
}

MnistException::~MnistException() noexcept
{}

const char *MnistException::what() const noexcept
{
    return msg.c_str();
}
