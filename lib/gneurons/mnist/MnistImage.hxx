#ifndef MNIST_IMAGE_HXX__
#define MNIST_IMAGE_HXX__

/*!
 * \file MnistImage.hxx
 * \brief Fichier d'en-tête permettant la manipulation de données d'images du MNIST.
 * 
 * \author Gaël Heizmann <g.heizmann@gmail.com>
 * \date 2020-11-12
 * \version 0.1
 */

#include <string>
#include <vector>
#include "MnistTypes.hxx"

/*!
 * \class MnistImage
 * \brief Cette classe permet de stocker et de manipuler des données issues
 * de fichiers images du MNIST
 */
class MnistImage
{
private:
    uchar **mnistData; /* Contient les données chargées du mnist */
    int nRows; /* Nombre de lignes d'une image */
    int nCols; /* Nombre de colonnes d'une image */
    int nImages; /* Nombre d'images */
    int imageSize; /* Taille d'une image */
public:
    /*!
     * \brief Constructeur
     */
    MnistImage();
    /*!
     * \brief Destructeur
     */
    ~MnistImage();
    /*!
     * \brief Méthode de chargement des données images du MNIST
     * \param fullPath Chemin vers le fichier (compressé) du MNIST à charger
     * \exception Lance une exception MnistException si des données sont déjà chargées ou bien si le fichier n'existe pas ou bien si le fichier n'a pas le bon magic number
     */
    void load(const std::string &fullPath);
    /*!
     * \brief Vide les données chargées, s'il y en a
     */
    void clear();
    /*!
     * \brief Obtenir une image
     * \param idx Le numéro de l'image à obtenir
     * \return Un vecteur contenant les pixels de l'image, avec une intensité de 0 à 255
     * \exception Lance une exception MnistException si aucune donnée n'est chargée ou bien si idx est out of range
     */
    const std::vector<uchar> operator[](int idx) const;
    /*!
     * \brief Obtenir le nombre de lignes d'une image
     * \return Le nombre de lignes d'une image (-1 si pas de données chargées)
     */
    int getNbRows() const;
    /*!
     * \brief Obtenir le nombre de colonnes d'une image
     * \return Le nombre de colonnes d'une image (-1 si pas de données chargées)
     */
    int getNbCols() const;
    /*!
     * \brief Obtenir le nombre d'images chargées
     * \return Le nombre d'images chargées (-1 si pas de données chargées)
     */
    int getNbImages() const;
    /*!
     * \brief Obtenir la taille d'une image
     * \return La taille d'une image (-1 si pas de données chargées)
     */
    int getImageSize() const;
};

#endif // MNIST_IMAGE_HXX__
