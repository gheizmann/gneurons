#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE GneuronsAlgorithmsTest

#include <boost/test/unit_test.hpp>
#include <vector>
#include <functional>

BOOST_AUTO_TEST_CASE(ConvertUcharToBinaryVectorTest)
{
    typedef unsigned char uchar;
    std::vector<uchar> in =
    {
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9
    };
    std::vector<std::vector<bool>> out =
    {
        {false, false, false, false},
        {false, false, false, true},
        {false, false, true, false},
        {false, false, true, true},
        {false, true, false, false},
        {false, true, false, true},
        {false, true, true, false},
        {false, true, true, true},
        {true, false, false, false},
        {true, false, false, true}
    };
    std::function<std::vector<bool>(uchar)> f =
    [](uchar nb) -> std::vector<bool>
    {
        std::vector<bool> ret(4);

        for(int i(3); i>=0; --i)
        {
            ret[i] = nb % 2;
            nb /= 2;
        }

        return ret;
    };

    for(int i(0); i<in.size(); ++i)
    {
        std::vector<bool> calc = f(in[i]);
        BOOST_CHECK_EQUAL(calc.size(), out[i].size());
        for(int j(0); j<calc.size(); ++j)
        {
            BOOST_CHECK_EQUAL(calc[j], out[i][j]);
        }
    }    
}
