#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE GneuronsNetworkTest

#include <boost/test/unit_test.hpp>
#include <algorithm>
#include <cmath>
#include "network/NetworkSettings.hxx"

BOOST_AUTO_TEST_CASE(randomTest)
{
    Gneurons::NetworkSettings &settings = Gneurons::NetworkSettings::getInstance();
    double nb;

    for(int i(0); i<1000000; ++i)
    {
        nb = settings.getRandom();
        BOOST_CHECK_GE(nb, 0.0);
        BOOST_CHECK_LT(nb, 1.0);
    }
}

BOOST_AUTO_TEST_CASE(learningRateTest)
{
    Gneurons::NetworkSettings &settings = Gneurons::NetworkSettings::getInstance();

    settings.setLearningRate(0.37);
    BOOST_CHECK_EQUAL(settings.getLearningRate(), 0.37);
    settings.setLearningRate(Gneurons::DEFAULT_LEARNING_RATE);
    BOOST_CHECK_EQUAL(settings.getLearningRate(), Gneurons::DEFAULT_LEARNING_RATE);
}

BOOST_AUTO_TEST_CASE(calcLossTest)
{
    Gneurons::NetworkSettings &settings = Gneurons::NetworkSettings::getInstance();
    std::vector<double> outCalc(1000);
    std::vector<double> outPred(1000);
    double tot { 0.0 };

    std::generate(outCalc.begin(), outCalc.end(), [&settings]() -> double
    {
        return settings.getRandom()-0.5;
    });
    std::generate(outPred.begin(), outPred.end(), [&settings]() -> double
    {
        return settings.getRandom()-0.5;
    });
    for(int i(0); i<outCalc.size(); ++i)
    {
        tot += std::pow(outCalc[i]-outPred[i], 2.0);
    }
    tot /= (2.0*outCalc.size());

    BOOST_CHECK_EQUAL(tot, settings.calcLoss(outCalc, outPred));
}

BOOST_AUTO_TEST_CASE(calcLossPrimeTest)
{
    Gneurons::NetworkSettings &settings = Gneurons::NetworkSettings::getInstance();
    std::vector<double> outCalc(1000);
    std::vector<double> outPred(1000);
    std::vector<double> inCalc(1000);
    std::vector<double> inPred;

    std::generate(outCalc.begin(), outCalc.end(), [&settings]() -> double
    {
        return settings.getRandom()*6.0-3.0;
    });
    std::generate(outPred.begin(), outPred.end(), [&settings]() -> double
    {
        return settings.getRandom()*6.0-3.0;
    });
    for(int i(0); i<inCalc.size(); ++i)
    {
        inCalc[i] = (outCalc[i]-outPred[i])/inCalc.size();
    }
    inPred = settings.calcLossPrime(outCalc, outPred);
    for(int i(0); i<inCalc.size(); ++i)
    {
        BOOST_CHECK_EQUAL(inCalc[i], inPred[i]);
    }
}
