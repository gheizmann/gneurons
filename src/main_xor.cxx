#include "main.hxx"

int main(int argc, char **argv)
{
    std::vector<std::vector<double>> trainingSet =
    {
        {0.0, 0.0},
        {0.0, 1.0},
        {1.0, 1.0},
        {1.0, 0.0}
    };
    std::vector<std::vector<double>> exitValues =
    {
        {0.0},
        {1.0},
        {0.0},
        {1.0}
    };
    Gneurons::NeuralNetwork neuralNetwork;
    
    // Build the neural layer
    neuralNetwork.add(new Gneurons::FcLayer(2, 3));
    neuralNetwork.add(new Gneurons::ActivationLayer(3, Gneurons::HYPERBOLIC_TANGENT));
    neuralNetwork.add(new Gneurons::FcLayer(3, 1));
    neuralNetwork.add(new Gneurons::ActivationLayer(1, Gneurons::HYPERBOLIC_TANGENT));
    // Settings
    Gneurons::NetworkSettings::getInstance().setNoise(0.001);
    Gneurons::NetworkSettings::getInstance().setLearningRate(0.025);
    // training
    for(int i(0); i<100000; ++i)
    {
        int val = (int)(Gneurons::NetworkSettings::getInstance().getRandom()*4.0);
        neuralNetwork.fit(trainingSet[val], exitValues[val]);
    }
    // test
    for(int i(0); i<trainingSet.size(); ++i)
    {
        std::cout << "Calc for entry: " << trainingSet[i][0] << "," << trainingSet[i][1] << ": ";
        std::vector<double> calc = neuralNetwork.predict(trainingSet[i]);
        std::cout << calc[0] << std::endl;
    }

    return EXIT_SUCCESS;
}
