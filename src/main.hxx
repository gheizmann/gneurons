#ifndef MAIN_HXX__
#define MAIN_HXX__

#include <cstdlib>
#include <iostream>
#include <string>
#include <algorithm>

#include "gneurons/mnist/MnistCurl.hxx"
#include "gneurons/mnist/MnistException.hxx"
#include "gneurons/mnist/MnistImage.hxx"
#include "gneurons/mnist/MnistLabel.hxx"
#include "gneurons/mnist/MnistTypes.hxx"
#include "gneurons/mnist/MnistVars.hxx"

#include "gneurons/network/AbstractLayer.hxx"
#include "gneurons/network/ActivationLayer.hxx"
#include "gneurons/network/ActivFunc.hxx"
#include "gneurons/network/ActivFuncTypes.hxx"
#include "gneurons/network/FcLayer.hxx"
#include "gneurons/network/GneuronsException.hxx"
#include "gneurons/network/NetworkSettings.hxx"
#include "gneurons/network/NeuralNetwork.hxx"

#endif // MAIN_HXX__
