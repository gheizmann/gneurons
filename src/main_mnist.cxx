#include "main.hxx"

int main(int argc, char **argv)
{
    Gneurons::NeuralNetwork neuralNetwork;
    Gneurons::NetworkSettings &settings = Gneurons::NetworkSettings::getInstance();
    MnistImage imageTrain;
    MnistLabel labelTrain;
    int countTest { 0 }; /* Compteur permettant de connaître le taux de fiabilité du réseau de neurones */

    // Téléchargement des fichiers du MNIST si pas téléchargés
    for(const std::string &file : MNIST_IMAGES_NAME)
    {
        if(!MnistCurl::isDownloaded(file))
        {
            std::cout << "File " << file << " is not downloaded." << std::endl;
            std::cout << "Download now: " << MNIST_URL_NAME << file << std::endl;
            MnistCurl::download(MNIST_URL_NAME, file);
        }
    }
    for(const std::string &file : MNIST_LABELS_NAME)
    {
        if(!MnistCurl::isDownloaded(file))
        {
            std::cout << "File " << file << " is not downloaded." << std::endl;
            std::cout << "Download now: " << MNIST_URL_NAME << file << std::endl;
            MnistCurl::download(MNIST_URL_NAME, file);
        }
    }
    // Chargement des données du MNIST (images et labels)
    imageTrain.load(MNIST_IMAGES_NAME[0]);
    labelTrain.load(MNIST_LABELS_NAME[0]);
    // Création du réseau de neurones
    neuralNetwork.add(new Gneurons::FcLayer(imageTrain.getImageSize(), 100));
    neuralNetwork.add(new Gneurons::ActivationLayer(100, Gneurons::HYPERBOLIC_TANGENT));
    neuralNetwork.add(new Gneurons::FcLayer(100, 50));
    neuralNetwork.add(new Gneurons::ActivationLayer(50, Gneurons::HYPERBOLIC_TANGENT));
    neuralNetwork.add(new Gneurons::FcLayer(50, 4));
    neuralNetwork.add(new Gneurons::ActivationLayer(4, Gneurons::HYPERBOLIC_TANGENT));
    // Configuration du réseau de neurones
    settings.setLearningRate(0.1);
    settings.setNoise(0.001);
    // Entraînement du réseau
    for(int i(0); i<imageTrain.getNbImages(); ++i)
    {
        std::vector<uchar> image = imageTrain[i];
        std::vector<bool> label = labelTrain[i];
        std::vector<double> in;
        std::vector<double> out;
        std::for_each(image.begin(), image.end(),
        [&in](uchar u) -> void
        {
            in.push_back((double)u / 255.0);
        });
        std::for_each(label.begin(), label.end(),
        [&out](bool b) -> void
        {
            out.push_back(b ? 1.0 : 0.0);
        });
        neuralNetwork.fit(in, out);
        if(i % 1000 == 0)
        {
            std::cout << "Training progress: step " << i << "/" << imageTrain.getNbImages() << std::endl;
        }
    }
    // Charger les données de test
    imageTrain.clear();
    labelTrain.clear();
    imageTrain.load(MNIST_IMAGES_NAME[1]);
    labelTrain.load(MNIST_LABELS_NAME[1]);
    // Vérifier si le réseau a appris quelque chose
    for(int i(0); i<imageTrain.getNbImages(); ++i)
    {
        std::vector<uchar> image = imageTrain[i];
        std::vector<bool> label = labelTrain[i];
        std::vector<double> in;
        std::vector<double> out;
        std::vector<double> pred;
        double calcRate;
        int predVal;
        std::for_each(image.begin(), image.end(),
        [&in](uchar u) -> void
        {
            in.push_back((double)u / 255.0);
        });
        std::for_each(label.begin(), label.end(),
        [&out](bool b) -> void
        {
            out.push_back(b ? 1.0 : 0.0);
        });
        pred = neuralNetwork.predict(in);
        calcRate = settings.calcLoss(out, pred);
        std::cout << "Rate: " << calcRate;
        std::cout << "\n\tTrue value: " << labelTrain.getLabel(i);
        std::cout << "\n\tPred value: ";
        predVal = 0;
        for(int j(0); j<pred.size(); ++j)
        {
            predVal *= 2;
            predVal += (pred[j] >= 0.5 ? 1 : 0);
        }
        std::cout << predVal << "\n";
        if(predVal == labelTrain.getLabel(i))
        {
            std::cout << "**\n";
            ++countTest;
        }
    }
    std::cout << "Percentage of success: " << (double)countTest / imageTrain.getNbImages() << std::endl;
    std::cout << "Nb of success: " << countTest << ", total nb img: " << imageTrain.getNbImages() << std::endl;

    return EXIT_SUCCESS;
}